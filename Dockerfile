FROM ubuntu:22.04

ENV DEBIAN_FRONTEND=noninteractive

ADD src /var/www/

EXPOSE 80

RUN apt-get update
RUN apt-get install -yq --no-install-recommends \
    # Install php 8.1
    php8.1 \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

RUN rm -rf /var/www/html 

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]